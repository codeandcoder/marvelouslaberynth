import java.util.List;


public class Main {

	private static List<Level> levels;
	
	public static void main(String[] args) {
		levels = LevelLoader.loadLevels();
		
		UIManager ui = new UIManager();
		Level currentLevel = levels.get(0);
		Player player = new Player(currentLevel.getStart());
		
		boolean finished = false;
		while(!finished) {
			ui.showLevel(currentLevel, player);
			
			Level.Square targetSquare = Level.Square.Wall;
			
			do {
				Coordinate move = ui.askDirection();
				Coordinate target = new Coordinate(player.getPosition().x + move.x, player.getPosition().y + move.y);
				
				targetSquare = currentLevel.getSquare(target);
				
				if (targetSquare == Level.Square.Wall) {
					ui.showMovedToWall();
					ui.showLevel(currentLevel, player);
				} else if (targetSquare == Level.Square.Exit) {
					finished = true;
					ui.showWin();
				} else {
					player.setPosition(target.x, target.y);
				}
			} while (targetSquare == Level.Square.Wall);
		}
		
	}
	
}
