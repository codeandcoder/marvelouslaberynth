
public class Player {
	
	public static final char REPRESENTATION = 'O';
	
	private Coordinate position;
	
	public Player(Coordinate initialPosition) {
		this.position = initialPosition;
	}

	public Coordinate getPosition() {
		return position;
	}
	
	public void setPosition(int x, int y) {
		this.position.x = x;
		this.position.y = y;
	}

}
