
public class Level {

	public static enum Square {Empty, Wall, Exit, Player}
	private String name;
	private Square[][] map;
	
	private Coordinate start;
	private Coordinate end;
	
	public Level(String name) {
		this.name = name;
	}
	
	public void buildLevel(char[][] layout) {
		map = new Square[layout.length][layout[0].length];
		
		for (int x = 0; x < layout.length; x++) {
			for (int y = 0; y < layout[0].length; y++) {
				char value = layout[x][y];
				if (value == 'O') {
					start = new Coordinate(y, x);
					map[y][x] = Square.Empty;
				} else if (value == 'S') {
					end = new Coordinate(y, x);
					map[y][x] = Square.Exit;
				} else if (value == '#') {
					map[y][x] = Square.Wall;
				} else {
					map[y][x] = Square.Empty;
				}
			}
		}
	}

	public String getName() {
		return name;
	}

	public Coordinate getStart() {
		return start;
	}

	public Coordinate getEnd() {
		return end;
	}
	
	public Square getSquare(int x, int y) {
		if (x < 0 || y < 0 || x >= map.length || y >= map[0].length) {
			return Square.Wall;
		}
		
		return map[x][y];
	}
	
	public Square getSquare(Coordinate coord) {
		if (coord.x < 0 || coord.y < 0 || coord.x >= map.length || coord.y >= map[0].length) {
			return Square.Wall;
		}
		
		return map[coord.x][coord.y];
	}
	
	public int getXSize() {
		return map.length;
	}
	
	public int getYSize() {
		return map[0].length;
	}
}
