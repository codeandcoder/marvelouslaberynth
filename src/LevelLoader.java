import java.util.ArrayList;
import java.util.List;


public class LevelLoader {

	public static List<Level> loadLevels() {
		List<Level> levels = new ArrayList<Level>();
		
		// FirstLevel
		Level firstLevel = new Level("First Level");
		firstLevel.buildLevel(new char[][] {{'#','#','#','#','#','#'},
											{'#','O','#','#',' ','S'},
											{'#',' ','#','#',' ','#'},
											{'#',' ',' ',' ',' ','#'},
											{'#',' ',' ','#',' ','#'},
											{'#','#','#','#','#','#'}});
		levels.add(firstLevel);
		
		return levels;
	}
	
}
