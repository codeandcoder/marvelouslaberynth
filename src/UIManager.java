import java.util.Scanner;


public class UIManager {

	public static final char FLOOR = ' ';
	public static final char WALL = '#';
	public static final char EXIT = 'S';
	
	private Scanner sc = new Scanner(System.in);
	
	public void showLevel(Level level, Player player) {
		for (int y = 0; y < level.getYSize(); y++) {
			for (int x = 0; x < level.getYSize(); x++) {
				if (x == player.getPosition().x && y == player.getPosition().y) {
					System.out.print(Player.REPRESENTATION);
				} else {
					Level.Square square = level.getSquare(x, y);
					if (square == Level.Square.Wall) {
						System.out.print(WALL);
					} else if (square == Level.Square.Exit){
						System.out.print(EXIT);
					} else {
						System.out.print(FLOOR);
					}
				}
			}
			System.out.println();
		}
	}
	
	public Coordinate askDirection() {
		System.out.println();
		System.out.println("Mueve al personaje ('W', 'A', 'S' o 'D' y presionar Enter)");
		System.out.print(">> ");
		char move = sc.next().charAt(0);
		System.out.println();
		
		switch(move) {
			case 'a': 	return new Coordinate(-1, 0);
			case 'd': 	return new Coordinate( 1, 0);
			case 'w': 	return new Coordinate( 0, -1);
			case 's': 	return new Coordinate( 0, 1);
		}
		
		return null;
	}
	
	public void showMovedToWall() {
		System.out.println("No puedes moverte en esa dirección, hay un muro.");
	}
	
	public void showWin() {
		System.out.println("¡Has finalizado el laberinto!");
	}
	
}
